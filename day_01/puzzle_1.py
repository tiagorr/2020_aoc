import requests
import settings

response = requests.get(
    'https://adventofcode.com/2020/day/1/input', headers={'cookie': settings.COOKIE})
values = response.text.splitlines()

for firstValue in values:
    for secondValue in values:
        if int(firstValue) + int(secondValue) == 2020:
            print(int(firstValue) * int(secondValue))
