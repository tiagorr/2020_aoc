import requests
import settings

passwords = requests.get('https://adventofcode.com/2020/day/2/input', headers={
                         "cookie": settings.COOKIE}).text

counter = 0
for password in passwords.split('\n')[:-1]:
    rules = password.split(':')[0].strip()
    userPass = password.split(':')[1].strip()

    limits = rules.split(' ')[0].split('-')
    ruleLetter = rules.split(' ')[1]

    letterCounter = userPass.count(ruleLetter)
    if letterCounter >= int(limits[0]) and letterCounter <= int(limits[1]):
        counter += 1

print(counter)
