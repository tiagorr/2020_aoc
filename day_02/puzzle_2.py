import requests
import settings

passwords = requests.get('https://adventofcode.com/2020/day/2/input', headers={
                         "cookie": settings.COOKIE}).text

counter = 0
for password in passwords.split('\n')[:-1]:
    rules = password.split(':')[0].strip()
    userPass = password.split(':')[1].strip()

    positions = [
        int(position) - 1 for position in rules.split(' ')[0].split('-')]
    ruleLetter = rules.split(' ')[1]

    letterIndexes = [item for item in range(
        0, len(userPass)) if userPass[item] == ruleLetter]
    isPosition = [True for index in letterIndexes if index in positions]

    if len(isPosition) == 1:
        counter += 1

print(counter)
