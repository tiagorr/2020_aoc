import requests
import settings
import statistics


def decodeSeat(section,  measurement, seat=0):
    interval = [0, measurement - 1]
    for half in section:
        median = int(statistics.median(interval))
        if half == 'L':
            interval = [interval[0], median]
            seat = interval[0]
        else:
            interval = [median + 1, interval[1]]
            seat = interval[1]
    return seat


seatsList = [seat.strip() for seat in requests.get('https://adventofcode.com/2020/day/5/input', headers={
    "cookie": settings.COOKIE}).text.split('\n')[:-1]]

planeLength = 128
planeWidth = 8

highestSeatId = 0

for seat in seatsList:
    rowSection = [row for row in seat[:7].replace('F', 'L').replace('B', 'U')]
    columnSection = [column for column in seat[7:].replace('R', 'U')]

    row = decodeSeat(rowSection, planeLength)
    column = decodeSeat(columnSection, planeWidth)

    seatId = row * 8 + column
    if seatId > highestSeatId:
        highestSeatId = seatId

print(highestSeatId)
