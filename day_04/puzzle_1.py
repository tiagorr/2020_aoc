import requests
import settings

passportList = [passport.strip() for passport in requests.get('https://adventofcode.com/2020/day/4/input', headers={
    "cookie": settings.COOKIE}).text.split('\n\n')]

requiredParameters = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
validCounter = 0

for passport in [' '.join(passport.split('\n')) for passport in passportList]:
    fields = [field.split(':')[0] for field in passport.split()]
    validFields = [
        True for parameter in requiredParameters if parameter in fields]
    if len(validFields) == len(requiredParameters):
        validCounter += 1

print(validCounter)
