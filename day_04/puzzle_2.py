import requests
import settings
import re

passportList = [passport.strip() for passport in requests.get('https://adventofcode.com/2020/day/4/input', headers={
    "cookie": settings.COOKIE}).text.split('\n\n')]

requiredParameters = {
    'byr': "(^19)[2-9][0-9]$|^(200)[0-2]$",
    'iyr': "^(20)(1[0-9]|20)$",
    'eyr': "^(20)(2[0-9]|30)$",
    'hgt': "(^1([5-8][0-9]|9[0-3])cm$)|(^(59|6[0-9]|7[0-6])in$)",
    'hcl': "^#[0-9a-f]{6}$",
    'ecl': "^((amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth))$",
    'pid': "^\d{9}$"
}
validCounter = 0

for passport in [' '.join(passport.split('\n')) for passport in passportList]:
    items = {}
    for field in [field for field in passport.split()]:
        items[field.split(':')[0]] = field.split(':')[1]

    isValid = [True for parameter in requiredParameters if parameter in items and re.search(
        requiredParameters[parameter], items[parameter])]
    if len(isValid) == len(requiredParameters):
        validCounter += 1

print(validCounter)
