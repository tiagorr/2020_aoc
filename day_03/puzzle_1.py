import requests
import settings

treeMap = requests.get('https://adventofcode.com/2020/day/3/input', headers={
    "cookie": settings.COOKIE}).text
treeLines = treeMap.split('\n')[:-1]
mapWidth = len(treeLines[0])

coordinates = [0, 0]
treeCounter = 0
while coordinates[0] < len(treeLines):
    if coordinates[1] >= mapWidth:
        coordinates[1] -= mapWidth

    currentSpace = treeLines[coordinates[0]][coordinates[1]]
    if currentSpace == "#":
        treeCounter += 1

    coordinates = [coordinates[0] + 1, coordinates[1] + 3]
print(treeCounter)
