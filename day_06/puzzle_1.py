import functools
import settings
import requests

groupAnswerList = [answer.strip() for answer in requests.get('https://adventofcode.com/2020/day/6/input', headers={
    "cookie": settings.COOKIE}).text.split('\n\n')]

totalCounter = []
for groupAnswers in groupAnswerList:
    alreadyAnswered = []
    for answers in groupAnswers.split('\n'):
        for answer in answers:
            if answer not in alreadyAnswered:
                alreadyAnswered.append(answer)
    totalCounter.append(len(alreadyAnswered))

print(functools.reduce(lambda x, y: x + y, totalCounter))
