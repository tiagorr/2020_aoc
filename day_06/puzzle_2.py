import settings
import requests

groupAnswerList = [answer.strip() for answer in requests.get('https://adventofcode.com/2020/day/6/input', headers={
    "cookie": settings.COOKIE}).text.split('\n\n')]

totalCounter = 0
for groupAnswers in groupAnswerList:
    alreadyAnswered = {}
    for answers in groupAnswers.split('\n'):
        for answer in answers:
            if answer not in alreadyAnswered:
                alreadyAnswered[answer] = 1
            else:
                alreadyAnswered[answer] += 1

    for answer in alreadyAnswered:
        if alreadyAnswered[answer] == len(groupAnswers.split('\n')):
            totalCounter += 1

print(totalCounter)
